" =====================================
" Vim Configuration
" Author: Aubhro Sengupta (lorddarkula)
" Email: hello@aubhro.com
" Website: https://aubhro.me
" =====================================


" Basic options
" -------------

" turns off legacy vi support
set nocompatible
" auto-detect filetype
filetype on
" allow backspace over autoindents, line breaks, start of indent
set backspace=indent,eol,start
" no beeping when end of line
set noerrorbells
" start searching while typing
set incsearch
" disable bell sound
set vb
" colors
syntax enable
" enable netrw and file type detection
filetype plugin on

" Appearance
" ----------

" syntax highlighting
syntax on
" line numbers
set number relativenumber

" Status line construction
let g:currentmode={
	\ 'n'  : 'NORMAL ',
	\ 'v'  : 'VISUAL ',
	\ 'V'  : 'V·Line ',
	\ '' : 'V·Block ',
	\ 'i'  : 'INSERT ',
	\ 'R'  : 'R ',
	\ 'Rv' : 'V·Replace ',
	\ 'c'  : 'Command ',
	\}
" permanently show status line
set laststatus=2
" display filename in statusline
set statusline=
set statusline+=\ %{toupper(get(g:currentmode,mode(),'V-Block'))}
set statusline+=\ %f
set statusline+=%{&modified?'[+]':''}
set statusline+=%=
set statusline+=\ %P
" enable cursor line
set cursorline
" use jellybeans if installed or else use elflord
exec "try\n	colorscheme jellybeans\ncatch\n	colorscheme elflord\nendtry"

" Indentation
" -----------
" turn on autoindent
set autoindent
" smarter automatic indenting
set smartindent
" stricter indent specifically for c
autocmd FileType c,cpp silent! set cindent

" Tabs and spaces
" ---------------
" tabs are hard tabs
set noexpandtab
" tabs show as 4 spaces by default
set tabstop=4
" shift > is equal to 4 spaces
set shiftwidth=4
" newline tabs and backspaces will be equivalent to shiftwidth
set smarttab

"Set soft tabs equivalent to 4 spaces in java,python
autocmd FileType py,python,java,rs,rust silent! set expandtab
"Set soft tabs equivalent to 2 spaces in javascript,scala
autocmd FileType js,scala,yml silent! set tabstop=2 expandtab shiftwidth=2

" Finding Files
" -------------
" Search through subdirectories
set path+=**
" Display matching file names when using tab complete
set wildmenu
" Ignore unimportant directories
set wildignore+=**/.git/**
set wildignore+=**/node_modules/**
set wildignore+=**/__pycache__/**
set wildignore+=**/*.out/**

" Map <F2> to open buffer list
nnoremap <F2> :ls<CR>

" Pane splitting
" --------------
" remaps pane navigation to Control + (h,j,k,l)
" nnoremap <C-H> <C-W><C-H>
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
nnoremap <C-J> <C-d>
nnoremap <C-K> <C-u>
nnoremap <Space><Space> <C-W><C-W>

" opens new panes below and to the right of currently open panes
set splitbelow
set splitright

" Highlighting
" ------------
" Highlight mutt config files
au BufRead,BufNewFile *.muttrc,*.mutt set filetype=muttrc

" whitespace hightlighted in white
highlight ExtraWhitespace ctermbg=white guibg=white
autocmd BufEnter * silent! 2match ExtraWhitespace /\s\+$/

" tabs highlighted in grey
highlight Tabs ctermbg=darkgrey guibg=darkgrey
autocmd BufEnter * silent! match Tabs /\t\+$/

" Additional Options
" ------------------
" auto-reload file after cursor stops moving
au CursorHold,CursorHoldI * checktime

" rename tmux tab name to filename
autocmd BufEnter,BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%:t"))
autocmd BufLeave,BufWinLeave * call system("tmux rename-window $(basename $SHELL)")

" load project-specific vim settings
silent! so .vimlocal

" disable arrow keys
noremap <Up>	<Nop>
noremap <Down> 	<Nop>
noremap <Left>	<Nop>
noremap <Right> <Nop>
