# dotfiles

My configuration files for bash, vim, and tmux.

## Installation

Clone this project into a directory on your computer.

```bash
$ git clone git@gitlab.com:LordDarkula/bash-config.git
```
Navigate to your home directory with `cd` and create symbolic links to each configuration file you want to use.

```bash
# Creates a symbolic link to .bashrc
$ ln -s /path/to/dotfiles/.bashrc .bashrc
```

## Usage

Whenever you want to make changes, edit the files in the `dotfiles` repo on your computer. The changes should automatically appear after restarting the shell.
