#!/bin/bash
# Dotfiles installation script

# Options: --bash, --dev, --term
INSTALL_TYPE="$1"

PROJECT_DIR=$(pwd)

# Paths to dotfile locations
BASHRC="$HOME/.bashrc"
BASH_PROFILE="$HOME/.bash_profile"
INPUTRC="$HOME/.inputrc"
VIMRC="$HOME/.vimrc"
TMUX_CONF="$HOME/.tmux.conf"

# Links configuration file to proper location
# Argumens:
# 	$1 - path of symlink to create
#	$2 - path of file in git repo
create_link() {
	if [ -f "$1" ]; then
		echo "File $1 exists"
		echo "Moving $1 to $1.old"
		mv "$1" "$1.old"
	fi
	ln "$2" "$1"
}

if [ $INSTALL_TYPE = "--zathura" ]; then
	ZATHURA_PATH="$HOME/.config/zathura/"
	mkdir -p $ZATHURA_PATH
	create_link "$ZATHURA_PATH/zathurarc" "zathura/zathurarc"
fi

# Installs bash configuration file
if [ $INSTALL_TYPE = "--bash" ]; then
	if [ -f "$BASHRC" ]; then
		echo ".bashrc exists"
		echo "Moving .bashrc to .bashrc.old"
		mv "$BASHRC" "$HOME/.bashrc.old"
	fi
	ln "$PROJECT_DIR/.bashrc" "$BASHRC"
	if [ -f "$BASH_PROFILE" ]; then
		echo ".bash_profile exists"
		echo "Moving .bash_profile to .bash_profile.old"
		mv "$BASH_PROFILE" "$HOME/.bash_profile.old"
	fi
	ln "$PROJECT_DIR/.bash_profile" "$BASH_PROFILE"
	if [ -f "$INPUTRC" ]; then
		echo ".inputrc exists"
		echo "Moving .inputrc to .inputrc.old"
		mv "$INPUTRC" "$HOME/.inputrc.old"
	fi
	ln "$PROJECT_DIR/.inputrc" "$INPUTRC"
fi

# Installs vim and tmux configuration files
if [ $INSTALL_TYPE = "--dev" ]; then
	if [ -f "$VIMRC" ]; then
		echo ".vimrc exists"
		echo "Moving .vimrc to .vimrc.old"
		mv "$VIMRC" "$HOME/.vimrc.old"
	fi
	ln "$PROJECT_DIR/.vimrc" "$VIMRC"
	if [ -f "$TMUX_CONF" ]; then
		echo ".tmux.conf exists"
		echo "Moving .tmux.conf to .tmux.conf.old"
		mv "$TMUX_CONF" "$HOME/.tmux.old"
	fi
	ln "$PROJECT_DIR/.tmux.conf" "$TMUX_CONF"
fi

# Installs ranger and alacritty configuration files
RANGER_DIR="$HOME/.config/ranger"
ALACRITTY_DIR="$HOME/.config/alacritty"
if [ $INSTALL_TYPE = "--term" ]; then
	mkdir -p "$RANGER_DIR"
	if [ -f "$RANGER_DIR/rc.conf" ]; then
		echo "rc.conf exists"
		echo "Moving rc.conf to rc.conf.old"
		mv "$RANGER_DIR/rc.conf" "$RANGER_DIR/rc.conf.old"
	fi
	if [ -f "$RANGER_DIR/rifle.conf" ]; then
		echo "rifle.conf exists"
		echo "Moving rifle.conf to rifle.conf.old"
		mv "$RANGER_DIR/rifle.conf" "$RANGER_DIR/rifle.conf.old"
	fi
	if [ -f "$RANGER_DIR/scope.sh" ]; then
		echo "scope.sh exists"
		echo "Moving scope.sh to scope.sh.old"
		mv "$RANGER_DIR/scope.sh" "$RANGER_DIR/scope.sh.old"
	fi
	ln "$PROJECT_DIR/ranger/rc.conf"	"$RANGER_DIR/rc.conf"
	ln "$PROJECT_DIR/ranger/rifle.conf"	"$RANGER_DIR/rifle.conf"
	ln "$PROJECT_DIR/ranger/scope.sh"	"$RANGER_DIR/scope.sh"

	mkdir -p "$ALACRITTY_DIR"
	if [ -f "$ALACRITTY_YML" ]; then
		echo "alacritty.yml exists"
		echo "Moving alacritty.yml to alacritty.yml.old"
		mv "$ALACRITTY_DIR/alacritty.yml" "$ALACRITTY_DIR/alacritty.yml.old"
	fi
	ln "$PROJECT_DIR/alacritty/alacritty.yml" "$ALACRITTY_DIR/alacritty.yml"
fi

